import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokeShopComponent } from './poke-shop.component';

describe('PokeShopComponent', () => {
  let component: PokeShopComponent;
  let fixture: ComponentFixture<PokeShopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokeShopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokeShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
