import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PokemonService } from '../pokemon.service';
import { UserService } from '../user.service';
import {empty} from 'rxjs';
import { Pokemon } from '../pokemon';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-poke-shop',
  templateUrl: './poke-shop.component.html',
  styleUrls: ['./poke-shop.component.scss']
})
export class PokeShopComponent implements OnInit {
  @Output() boughtBooster: EventEmitter<any> = new EventEmitter();
  @Output() pokepoints: number;
  boosterPrice = 10;
  pokemons = [];
  canBuy = true;
  user: "";
  
  constructor(private pokemonService: PokemonService, private userService: UserService) { }

  ngOnInit() {
    this.loadUser();
    this.loadMoney();
  }

  /* Crée un tableau d'objets pokemon, qui se vide a chaque clique */
  buyBooster() {
    if(this.canBuy && this.pokepoints >= this.boosterPrice)
    {
      this.pokepoints -= this.boosterPrice;
      this.saveMoney();
      this.canBuy = false;
      this.pokemons = [];
      for(let i = 0; i < 10; i++)
      {
        this.pokemons.push({});
        this.pokemonService.randomPokemon().subscribe(pokemon => {
          //console.log(pokemon);
          this.pokemons[i] = pokemon;
          if (i === 9) this.canBuy = true;
        });
      }
    }
  }

  removePokemon(index, moneyBack = true) {
    this.pokemons.splice(index, 1);
    if(moneyBack)
    {
      this.pokepoints++;
      this.saveMoney();
    }
  }

  saveMoney()
  {
    localStorage.setItem("user_money", JSON.stringify({ pokepoints: this.pokepoints }));
    this.userService.setMoney(this.user, this.pokepoints).subscribe(res => {
      console.log(res);
    });
  }

  loadMoney()
  {
    let dataSource = localStorage.getItem("user_money"); 
    
    if(dataSource)
    {
      let data = JSON.parse(dataSource);
      this.pokepoints = data.pokepoints; 
    }
  }

  loadUser()
  {
    let dataSource = localStorage.getItem("user"); 
    
    if(dataSource)
    {
      let data = JSON.parse(dataSource);
      this.user = data.login;
    }
  }

  addToDeck(pokemonIndex)
  {
    if(pokemonIndex != undefined)
    {
      if(this.user && this.user != "")
      {
        var id = this.pokemons[pokemonIndex].poke_id;
        this.removePokemon(pokemonIndex, false);
        this.pokemonService.addToDeck(id, this.user).subscribe(function(res) {
          console.log(res);
        });
      }
    }
  }

}
