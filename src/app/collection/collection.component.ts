import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PokemonService } from '../pokemon.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {

  user = "";
  @Input() pokemons = [];
  @Input() cardsOnly = false;
  @Output() grabClicked: EventEmitter<any> = new EventEmitter();
  @Output() crossClicked: EventEmitter<any> = new EventEmitter();

  constructor(private pokemonService: PokemonService, private modalService: NgbModal) {

  }
  ngOnInit(){
    if(!this.cardsOnly)
    {
      this.loadUser();

      if(this.user)
      {
        this.pokemonService.getDeck(this.user).subscribe(deck => {
          if(deck)
          {
            this.pokemons = [];
            for(let i = 0; i < deck.pokemons.length; i++)
            {
              this.pokemons.push({});
              this.pokemonService.getPokemon(deck.pokemons[i]).subscribe(pokemon => {
                this.pokemons[i] = pokemon;
              });
            }
          }
        });
      }
    }
  }

  loadUser()
  {
    let dataSource = localStorage.getItem("user"); 
    
    if(dataSource)
    {
      let data = JSON.parse(dataSource);
      this.user = data.login;
    }
  }

  open(content) {
    this.modalService.open(content);
  }

  addToDeck(pokemonIndex)
  {
    this.grabClicked.emit(pokemonIndex);
  }

  delegateRemovePokemon(pokemonIndex)
  {
    this.crossClicked.emit(pokemonIndex);
  }

  removePokemon(pokemonIndex) {
    this.pokemonService.removeFromDeck(this.pokemons[pokemonIndex].poke_id, this.user).subscribe(function(deck) {
      console.log(deck);
    });
    this.pokemons.splice(pokemonIndex, 1);
  }

}
