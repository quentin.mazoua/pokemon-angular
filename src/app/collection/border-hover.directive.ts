import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appBorderHover]'
})
export class BorderHoverDirective {

  private initialColor: string = '#f5f5f5';
	private defaultColor: string = '#dc3545';

	constructor(private el: ElementRef) {
		this.setBorder(this.initialColor);
	}

	@Input('pkmnBorderCard') borderColor: string;

	@HostListener('mouseenter') onMouseEnter() {
		this.setBorder(this.borderColor || this.defaultColor);
	}

	@HostListener('mouseleave') onMouseLeave() {
		this.setBorder(this.initialColor);
	}

	private setBorder(color: string) {
		let border = 'solid 4px ' + color;
		this.el.nativeElement.style.border = border;
	}
}
