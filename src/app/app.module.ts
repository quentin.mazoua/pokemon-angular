import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { CollectionComponent } from './collection/collection.component';
import { PokemonTypeColorPipe } from './pokemon-type-color.pipe';
import { BorderHoverDirective } from './collection/border-hover.directive';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { PokeShopComponent } from './poke-shop/poke-shop.component';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './navbar/navbar.component';
import { RegisterComponent } from './register/register.component';
import {MatProgressBarModule} from '@angular/material';
import {MatTooltipModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { CollectionFilterPipe } from './collection-filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    CollectionComponent,
    PokemonTypeColorPipe,
    BorderHoverDirective,
    PokeShopComponent,
    NavbarComponent,
    RegisterComponent,
    CollectionFilterPipe
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatProgressBarModule,
    MatTooltipModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
