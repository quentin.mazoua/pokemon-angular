import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  login: string;
  password: string;
  avatar: string;
  pokepoints: Number;
  error = "";
  loading = false;

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
  }

  checkLogin() 
  {
    event.preventDefault();
    this.loading = true;

    this.userService.auth(this.login, this.password).subscribe((res) => {
      this.loading = false;
      if(res.length > 0)
      {
        this.avatar = res[0].avatar;
        this.pokepoints = res[0].pokepoints;
        this.saveLogin();
        this.router.navigateByUrl('collection');
      }
      else 
      {
        this.error = "Login and/or password incorrect";
      }
      console.log(res);
    });
  }

  saveLogin()
  {
    localStorage.setItem('user', JSON.stringify({ login: this.login, avatar: this.avatar, pokepoints: this.pokepoints }));
    localStorage.setItem("user_money", JSON.stringify({ pokepoints: this.pokepoints }));
  }
}
