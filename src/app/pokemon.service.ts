import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pokemon } from './pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  constructor(private http: HttpClient) { }

  randomPokemon(): Observable<Pokemon> {
      const id = Math.ceil(Math.random() * 200);
      return this.http.get<Pokemon>('https://lpweblannion.herokuapp.com/api/pokemon/' + id);
  }

  getPokemon(id: number): Observable<Pokemon> {
    return this.http.get<Pokemon>('https://lpweblannion.herokuapp.com/api/pokemon/' + id);
  }

  addToDeck(pokemonID, userLogin): Observable<any> {
    return this.http.post<any>('http://localhost:8080/api/pokemon/add', { pokemon: pokemonID, user: userLogin });
  }

  getDeck(userLogin): Observable<any> {
    return this.http.get<any>('http://localhost:8080/api/utilisateur/'+ userLogin +'/deck');
  }

  removeFromDeck(pokemonID, userLogin ): Observable<any> {
    return this.http.post<any>('http://localhost:8080/api/utilisateur/deck/remove', { pokemon: pokemonID, user: userLogin });
  }
}
