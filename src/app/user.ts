export interface User 
{
    id: String;
    login: String;
    pokepoints: Number;
    avatar: String;
}
