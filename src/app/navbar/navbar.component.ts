import { Component, OnInit, Input } from '@angular/core';
import { Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private router: Router) { }
  @Input() argent = 100;
  title = 'pokemon';
  @Input() pseudo : string;
  @Input() avatar : string;

  decreaseMoney(amount) {
    this.argent -= Math.abs(amount);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData()
  {
    let dataSource = localStorage.getItem("user_money"); 
    
    if(dataSource)
    {
      let data = JSON.parse(dataSource);
      this.argent = data.pokepoints;
    }

    let dataSource2 = localStorage.getItem("user");
    console.log(dataSource2);

    if(dataSource2){
      let data2 = JSON.parse(dataSource2);
      this.pseudo = data2.login;
      this.avatar = data2.avatar;
      console.log(this.pseudo);
    }

  }

  removeUser()
  {
    localStorage.removeItem("user");
    localStorage.removeItem("user_money");
    this.router.navigateByUrl('login');
  }

  computeAvatar(avatar)
  {
    return (avatar && avatar != "") ? avatar : "assets/pokemon_image/placeholder-avatar.jpg";
  }

}
