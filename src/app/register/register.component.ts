import { Component, OnInit } from '@angular/core';
import { UserForm } from '../user-form';
import { FileUpload } from '../file-upload';
import { UserService } from '../user.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  file: FileUpload;
  form = new UserForm();
  error = "";

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
    this.file = { filename: "", filetype: "", value: "" } 
  }

  computePasswordStrength(password: String)
  {
    var score = 0;

    if(password != undefined)
    {
      // If the password length is equal to 0
      if(password.length == 0) score = 0;

      // If the password length is less than or equal to 6
      else if(password.length<=6) score = 25;

      // If the password length is greater than 6 and contain any lowercase alphabet or any number or any special character
      if(password.length>6 && (password.match(/[a-z]/) || password.match(/\d+/) || password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))) score = 50;

      // If the password length is greater than 6 and contain alphabet,number,special character respectively
      if(password.length>6 && ((password.match(/[a-z]/) && password.match(/\d+/)) || (password.match(/\d+/) && password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) || (password.match(/[a-z]/) && password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)))) score = 75;

      // If the password length is greater than 6 and must contain alphabets,numbers and special characters
      if(password.length>6 && password.match(/[a-z]/) && password.match(/\d+/) && password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) score = 100;
    }
    
    return score;
  }

  onFileChange(event)
  {
    let reader = new FileReader();
    let file;

    if(event.target.files.length > 0)
    {
      file = event.target.files[0];

      if(file.type.includes("image"))
      {
        reader.readAsDataURL(file); // Encodage du fichier en base64
      
        reader.onload = () => {
          this.file.filename = file.name;
          this.file.filetype = file.type;
          this.file.value = reader.result.toString().split(',')[1];
          console.log(this.file);
        };
      }
    }
  }

  submitForm()
  {
    console.log(this.form, this.file);

    if(this.form.password && this.form.passwordCheck && this.form.login)
    {
      if(this.form.password === this.form.passwordCheck)
      {
        this.userService.register(this.form.login, this.form.password, this.form.surname, this.form.name, this.file).subscribe((res) => {
          if(!res.errmsg) this.router.navigateByUrl('login');
          else if(res.code == 11000) this.error = 'Le login est déjà utilisé';
        });
      }
    }
  }

  computePasswordCheckClass(password, passwordCheck)
  {
    var classes = 'form-control ';

    if(password && passwordCheck)
    {
      classes += password == passwordCheck ? 'valid' : 'invalid'; 
    }
    return classes;
  }

}
