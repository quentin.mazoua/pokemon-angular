export class UserForm {
    login: string;
    password: string;
    passwordCheck: string;
    surname: string;
    name: string;
}
