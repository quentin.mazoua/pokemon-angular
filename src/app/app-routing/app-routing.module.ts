import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CollectionComponent } from '../collection/collection.component';
import { LoginFormComponent } from '../login-form/login-form.component';
import { PokeShopComponent } from '../poke-shop/poke-shop.component';
import { RegisterComponent } from '../register/register.component';

// import { PageNotFoundComponent } from './page-not-found.component';

// routes
const appRoutes: Routes = [
	{ path: 'collection', component: CollectionComponent },
	{ path: 'login', component: LoginFormComponent },
  { path: 'register', component: RegisterComponent },
	{ path: 'pokeShop', component :  PokeShopComponent },
	{ path: '', redirectTo: 'login', pathMatch: 'full' },

	// { path: '**', component: PageNotFoundComponent }
];

@NgModule({
	imports: [
		RouterModule.forRoot(appRoutes)
	],
	exports: [
		RouterModule
	]
})
export class AppRoutingModule { }
