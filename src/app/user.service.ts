import { Injectable } from '@angular/core';
import { User } from './user';
import { FileUpload } from './file-upload';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService 
{

  constructor(private http: HttpClient) { }

  auth(login: String, password: String): Observable<any> {
    return this.http.post<User>('http://localhost:8080/api/utilisateur/auth', { login: login, password: password });
  }

  register(login: string, password: string, surname = "", name = "", avatar: FileUpload): Observable<any> {
    return this.http.post<User>('http://localhost:8080/api/utilisateurs', { login: login, password: password, surname: surname, name: name, avatar: avatar });
  }

  setMoney(login: string, pokepoints: number): Observable<any> {
    return this.http.post<User>('http://localhost:8080/api/utilisateur/money', { login: login, pokepoints: pokepoints });
  }
}
