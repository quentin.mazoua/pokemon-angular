import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pokemonTypeColor'
})
export class PokemonTypeColorPipe implements PipeTransform {
/* Vérifie le type et marie le type avec un coleur de background */
  transform(type: string): string {
    let color: string;
    switch (type) {
      case 'fire':
        color = 'fire';
        break;
      case 'water':
        color = 'badge-primary';
        break;
      case 'ice':
        color = 'ice';
        break;
      case 'grass':
        color = 'badge-success';
        break;
      case 'bug':
        color = 'bug';
        break;
      case 'normal':
        color = 'badge-secondary';
        break;
      case 'flying':
        color = 'fly';
        break;
      case 'poison':
        color = 'poison';
        break;
      case 'fairy':
        color = 'fairy';
        break;
      case 'psychic':
        color = 'psy';
        break;
      case 'steel':
        color = 'steel';
        break;
      case 'ghost':
        color = 'ghost';
        break;
      case 'dark':
        color = 'badge-dark';
        break;
      case 'electric':
        color = 'badge-warning';
        break;
      case 'fighting':
        color = 'badge-danger';
        break;
      case 'ground':
        color = 'ground';
        break;
      case 'rock':
        color = 'rock';
        break;
      case 'dragon':
        color = 'rock';
        break;
      default:
        color = 'badge-secondary';
        break;
    }

    return 'badge badge-pill ' + color;

  }

}
