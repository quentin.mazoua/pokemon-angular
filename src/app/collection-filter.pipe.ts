import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'collectionFilter'
})
export class CollectionFilterPipe implements PipeTransform {

  transform(value: any, minAtk): any {
    if(minAtk != undefined) return value.filter(pokemon => { return (pokemon.stats == undefined) || pokemon.stats.attack >= minAtk; });
    return value;
  }

}
